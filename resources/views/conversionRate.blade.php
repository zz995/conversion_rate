@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Коэффициент конверсии страниц</div>

        <table class="employees table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Страница</th>
                    <th>За сегодня</th>
                    <th>За вчера</th>
                    <th>За неделю</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pages as $page)
                    <tr>
                        <td>{{ $page->name }}</td>
                        <td>{{ $page->today_cr }}</td>
                        <td>{{ $page->yesterday_cr }}</td>
                        <td>{{ $page->week_cr }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection