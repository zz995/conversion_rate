<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <span class="navbar-brand">
                    {{ config('app.name', 'Laravel') }}
                </span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li>
                            <a class="nav-link {{ Route::currentRouteNamed('conversionRate') ? 'active' : '' }}" href="{{ route('conversionRate') }}">
                                Коэффициент конверсии
                            </a>
                        </li>
                        <li>
                            <a class="nav-link {{ Route::currentRouteNamed('page1') ? 'active' : '' }}" href="{{ route('page1') }}">
                                Страница 1
                            </a>
                        </li>
                        <li>
                            <a class="nav-link {{ Route::currentRouteNamed('page2') ? 'active' : '' }}" href="{{ route('page2') }}">
                                Страница 2
                            </a>
                        </li>
                        <li>
                            <a class="nav-link {{ Route::currentRouteNamed('page3') ? 'active' : '' }}" href="{{ route('page3') }}">
                                Страница 3
                            </a>
                        </li>
                        <li>
                            <a class="nav-link {{ Route::currentRouteNamed('page4') ? 'active' : '' }}" href="{{ route('page4') }}">
                                Страница 4
                            </a>
                        </li>
                        <li>
                            <a class="nav-link {{ Route::currentRouteNamed('page5') ? 'active' : '' }}" href="{{ route('page5') }}">
                                Страница 5
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main class="app-content py-4">
        <div class="container container_white">
            @include('partials.flash.flash')
            @yield('content')
        </div>
    </main>
</body>
</html>
