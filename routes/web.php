<?php

use App\Http\Middleware\VisitPage;

Route::get('/page1', 'PageController@page1')->name('page1')->middleware(VisitPage::class);
Route::post('/page1', 'PageController@create1');
Route::get('/page2', 'PageController@page2')->name('page2')->middleware(VisitPage::class);
Route::post('/page2', 'PageController@create2');
Route::get('/page3', 'PageController@page3')->name('page3')->middleware(VisitPage::class);
Route::post('/page3', 'PageController@create3');
Route::get('/page4', 'PageController@page4')->name('page4')->middleware(VisitPage::class);
Route::post('/page4', 'PageController@create4');
Route::get('/page5', 'PageController@page5')->name('page5')->middleware(VisitPage::class);
Route::post('/page5', 'PageController@create5');

Route::get('/', 'PageController@conversionRate')->name('conversionRate');