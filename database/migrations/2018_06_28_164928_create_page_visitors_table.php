<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageVisitorsTable extends Migration
{

    public function up()
    {
        Schema::create('page_visitors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('timestamp');
            $table->string('user_agent', 512);
            $table->string('referer', 2048)->nullable();
            $table->string('ip', 15);
            $table->unsignedInteger('page_id');

            $table
                ->foreign('page_id')
                ->references('id')
                ->on('pages')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists('page_visitors');
    }
}
