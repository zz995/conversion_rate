<?php

namespace App\Entity\Page;

use App\Entity\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property double $today_cr
 * @property double $yesterday_cr
 * @property double $week_cr
 * @property Carbon $create_at
 * @property Carbon $update_at
 */
class Page extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $casts = [
        'today_cr' => 'double',
        'yesterday_cr' => 'double',
        'week_cr' => 'double',
    ];

    public function pageVisitors()
    {
        return $this->hasMany(PageVisitor::class, 'page_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'page_id', 'id');
    }

    static public function getByPath(string $path): Page
    {
        $path = '/' . ltrim($path, '/');

        /** @var Page $page */
        $page = Page::firstOrCreate(array('name' => $path));
        return $page;
    }
}