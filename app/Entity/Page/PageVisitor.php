<?php

namespace App\Entity\Page;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property Carbon $timestamp
 * @property string $user_agent
 * @property string|null $referer
 * @property string $ip
 */
class PageVisitor extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'timestamp', 'user_agent', 'referer', 'ip'
    ];

    protected $casts = [
        'timestamp' => 'datetime',
    ];

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }

    public function scopeToday(Builder $query)
    {
        return $query->where('timestamp', '>', Carbon::today()->toDateString());
    }

    public function scopeYesterday(Builder $query)
    {
        return $query
            ->where('timestamp', '>', Carbon::yesterday()->toDateString())
            ->where('timestamp', '<=', Carbon::today()->toDateString());
    }

    public function scopeWeek(Builder $query)
    {
        return $query->where('timestamp', '>', Carbon::now()->startOfWeek());
    }

    static public function writeVisit($userAgent, $referer, $ip, $path)
    {
        $page = Page::getByPath($path);

        $page->pageVisitors()->create([
            'user_agent' => $userAgent,
            'referer' => $referer,
            'ip' => $ip,
            'timestamp' => Carbon::now()->toDateTimeString()
        ]);
    }
}