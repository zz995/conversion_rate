<?php

namespace App\Entity;

use App\Entity\Page\Page;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property Carbon $create_at
 * @property Carbon $update_at
 */
class Order extends Model
{
    protected $fillable = [
        'name', 'phone'
    ];

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }

    public function scopeToday(Builder $query)
    {
        return $query->where('created_at', '>', Carbon::today()->toDateString());
    }

    public function scopeYesterday(Builder $query)
    {
        return $query
                ->where('created_at', '>', Carbon::yesterday()->toDateString())
                ->where('created_at', '<=', Carbon::today()->toDateString());
    }

    public function scopeWeek(Builder $query)
    {
        return $query->where('created_at', '>', Carbon::now()->startOfWeek());
    }

    static public function shape(string $name, string $phone, string $path): Order
    {
        $page = Page::getByPath($path);
        /** @var Order $order */
        $order = $page->orders()->create(['name' => $name, 'phone' => $phone]);
        return $order;
    }
}