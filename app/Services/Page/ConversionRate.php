<?php

namespace App\Services\Page;

use App\Entity\Page\Page;
use Carbon\Carbon;

class ConversionRate
{
    public function calc()
    {
        /** @var Page $page */
        foreach (Page::all() as $page) {
            $page->today_cr = $this->conversionRate($page->orders()->today()->count(), $page->pageVisitors()->today()->count());
            $page->yesterday_cr = $this->conversionRate($page->orders()->yesterday()->count(), $page->pageVisitors()->yesterday()->count());
            $page->week_cr = $this->conversionRate($page->orders()->week()->count(), $page->pageVisitors()->week()->count());
            $page->save();
        }
    }

    private function conversionRate(int $orderCount, int $visitCount): float
    {
        if ($visitCount == 0) {
            return 0;
        }

        return $orderCount / $visitCount;
    }
}