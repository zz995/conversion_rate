<?php

namespace App\Http\Controllers;

use App\Entity\Order;
use App\Entity\Page\Page;
use App\Http\Requests\CreateOrderRequest;

class PageController extends Controller
{
    public function page1()
    {
        return view('page');
    }

    public function create1(CreateOrderRequest $request)
    {
        Order::shape($request->get('name'), $request->get('phone'), $request->path());
        return back()->with('success', 'Заказ успешно создан');
    }

    public function page2()
    {
        return view('page');
    }

    public function create2(CreateOrderRequest $request)
    {
        Order::shape($request->get('name'), $request->get('phone'), $request->path());
        return back()->with('success', 'Заказ успешно создан');
    }

    public function page3()
    {
        return view('page');
    }

    public function create3(CreateOrderRequest $request)
    {
        Order::shape($request->get('name'), $request->get('phone'), $request->path());
        return back()->with('success', 'Заказ успешно создан');
    }

    public function page4()
    {
        return view('page');
    }

    public function create4(CreateOrderRequest $request)
    {
        Order::shape($request->get('name'), $request->get('phone'), $request->path());
        return back()->with('success', 'Заказ успешно создан');
    }

    public function page5()
    {
        return view('page');
    }

    public function create5(CreateOrderRequest $request)
    {
        Order::shape($request->get('name'), $request->get('phone'), $request->path());
        return back()->with('success', 'Заказ успешно создан');
    }

    public function conversionRate()
    {
        $pages = Page::all();
        return view('conversionRate', compact('pages'));
    }
}
