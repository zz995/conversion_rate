<?php

namespace App\Http\Middleware;

use App\Jobs\VisitPage as VisitPageJob;
use Illuminate\Http\Request;

class VisitPage
{
    public function handle(Request $request, \Closure $next)
    {
        VisitPageJob::dispatch(
            $request->header('User-Agent'),
            $request->headers->get('referer'),
            $request->ip(),
            $request->path()
        );

        return $next($request);
    }
}
