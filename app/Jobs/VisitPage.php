<?php

namespace App\Jobs;

use App\Entity\Page\PageVisitor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class VisitPage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $userAgent;
    private $referer;
    private $ip;
    private $path;

    public function __construct($userAgent, $referer, $ip, $path)
    {
        $this->userAgent = $userAgent;
        $this->referer = $referer;
        $this->ip = $ip;
        $this->path = $path;
    }

    public function handle()
    {
        PageVisitor::writeVisit(
            $this->userAgent,
            $this->referer,
            $this->ip,
            $this->path
        );
    }
}
